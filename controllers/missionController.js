var missionDB = require('../models/mission')
    // var levelController = require('./levelController')
var userDB = require('../models/user')
var jobModel = require('../models/jobs')
var moment = require('moment')
var job = require('../jobs/missions/min-level')

exports.verify = async(data, cb) => {
    var mission = data
    if (mission.length >= 1)
        mission = data[0]
    if (mission._id == undefined || mission._id == "") {
        return
    }

    var fn = job[mission.function]
    if (typeof fn === "function") {
        await fn.apply(null, [data, cb])
    }
}

exports.done = (req, start, job) => {
    var mission = req
    if (mission.length >= 1)
        mission = req[0]
    jobModel.consumedJob(job._id, job, start, (err, job) => {})
    var log = {
        missionID: mission._id,
        userID: mission.currentUser,
        status: "DONE",
        completedTime: moment().format()
    }
    missionDB.done(log, (err, l) => {

    })
}