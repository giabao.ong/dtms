var authController = require('../controllers/authController')
var commentDB = require('../models/comment')
var moment = require('moment')

exports.create = (req, next) => {
    authController.auth(req, (err, data) => {
        if (data) { // currentUser
            var newComment = {
                userID: data._id,
                name: data.name,
                avatar: data.avatar,
                postID: req.postID,
                content: req.content || "",
                createdTime: moment().format()
            }
            var res = {}
            commentDB.create(newComment, (err, cmt) => {
                if (cmt) {
                    res.newComment = cmt
                    res.currentUser = data
                    return next(null, res)
                } else {
                    return next({ status: "failed" }, null)
                }
            })
        } else {
            return next({ status: "failed" }, null)
        }
    })
}