const jwt = require('jsonwebtoken');
var userDB = require('../models/user')
require('dotenv').config();


exports.verify = (req, send) => {
    this.auth(req, (err, data) => {
        return send(err, data)
    })
}

exports.auth = (req, next) => {
    var verify = jwt.verify(req.token, process.env.SECRET_KEY, (err, user) => {
        if (user) {
            this.getUserCurrent(user, (err, data) => {
                if (data != null) {
                    return next(null, data)
                }
            })
        } else {
            //can't get userID
            return next({ status: "failed" }, null)
        }
    });
}

exports.getUserCurrent = (req, next) => {
    userDB.getFromId(req._id, (err, user) => {
        if (user) {
            return next(null, user)
        } else {
            return next({ status: "failed" }, null)

        }
    })
}