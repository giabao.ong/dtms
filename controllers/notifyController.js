var notifyDB = require('../models/notify')
var moment = require('moment')
var authController = require('./authController')

exports.create = (data, next) => {

    notifyDB.create(data, (err, notify) => {
        return next(err, notify)
    })
}

exports.push = (req, next) => {
    this.create(req, (err, notify) => {
        if (notify) {
            return next(null, notify)
        } else {
            return next({ status: "failed" })
        }
    })
}

exports.getAny = (req, res) => {
    var query = req.query.q
    var q = JSON.parse(query)
    var limit = parseInt(req.query.limit) || 1;
    var offset = parseInt(req.query.offset) || 0;
    var reverse = (String(req.query.reverse) === "true");

    notifyDB.getAny(q, limit, offset, reverse, (err, notify) => {
        if (err) {
            return res.status(500).send({ status: 400, message: err.message })
        } else {
            return res.status(200).send({ status: 200, message: "Created successfully", data: notify })
        }
    })
}