var moment = require('moment')
var likeDB = require('../models/like')
var authController = require('../controllers/authController')

exports.like = (req, next) => {
    authController.auth(req, (err, data) => {
        if (data) { // currentUser
            var newLike = {
                userID: data._id,
                name: data._name,
                avatar: data._avatar,
                postID: req.postID,
                createdTime: moment().format()
            }
            var res = {}
            likeDB.like(newLike, (err, like) => {
                if (like) {
                    res.newLike = like
                    res.currentUser = data
                    return next(null, res)
                } else {
                    return next({ status: "failed" }, null)
                }
            })
        } else {
            return next({ status: "failed" }, null)
        }
    })
}

exports.unLike = (req, res) => {
    var currentUser = req.currentUser;
    var data = {
        userID: currentUser._id,
        postID: req.query.postID,
    }
    likeDB.unLike(data, (err, like) => {
        if (err)
            return res.status(500).send({ status: 500, message: err })
        if (like)
            return res.status(200).send({ status: 200, message: "UnLiked", data: like })
    })

}