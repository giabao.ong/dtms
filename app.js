var express = require('express');
var cors = require('cors')
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require("compression")
var dbConfig = require('./config/db')
var axios = require('axios')
require('dotenv').config();
const passport = require('passport');
require('./config/passport')(passport);
var app = express();

dbConfig.initDB
dbConfig.initDBLog

const version = "v1"
const appName = "dtms"
const url = "/" + appName + "/" + version

app.set('url', url)

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
// uncomment after placing your favicon in /public
app.use(compression());
app.set("env", process.env.NODE_ENV || "development");
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors())

// các cài đặt cần thiết cho passport

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Credentials', 'true')
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT')
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next()
})


app.get('/', (req, res) => {
    res.status(200).send("DTMS started");
})

app.get(url, (req, res) => {
    res.status(200).send("DTMS started");
});

process.on('uncaughtException', (err, origin) => {
    axios.post(`http://api.telegram.org/bot1319027140:AAEC7QwlZRh_Vbygv352GtLwmc1gDa5a2a0/sendMessage?chat_id=-1001200490767&text= ! DTMS crashed ${err.stack}`)
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    if (err.status == 500) {
        axios.post(`http://api.telegram.org/bot1319027140:AAEC7QwlZRh_Vbygv352GtLwmc1gDa5a2a0/sendMessage?chat_id=-1001200490767&text= ! DTMS crashed ${err.stack}`)
    }
    res.status(err.status || 500).send({ message: err.message });
});


module.exports = app;