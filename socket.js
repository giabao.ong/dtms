var io = require('socket.io')
const jwt = require('jsonwebtoken');
var app = require('./app')
var moment = require('moment')
var ChatroomDB = require('./models/chatroom')
var MessageDB = require("./models/message");
var likeController = require('./controllers/likeController')
var commentController = require('./controllers/commentController')
var authController = require('./controllers/authController')
var notifyController = require('./controllers/notifyController')

const listener = (port, next) => io.listen(port)

const init = socket => {
    if (socket != undefined) {
        socket.removeAllListeners();
    }

    socket.of(app.get('url')).on('connection', socketIO => {
        socket.removeAllListeners();

        socketIO.on('error', data => {
            console.log(data)
        })

        var socketID = socketIO.id
        socketIO.rooms = []
        socketIO.listRooms = []
            //app connect
        if (socketIO.handshake.headers["x-apikey"] === "QkBvMjcxMTE5OTg=") {
            socketIO.on('authenticator', req => {
                authController.verify(req, (err, data) => {
                    if (data != null) {
                        if (String(data._id) != String(socketIO.rooms["user"]) || !socketIO.listRooms.includes(data._id)) {
                            socketIO.currentUser = data._id
                            socketIO.rooms["user"] = data._id
                            socketIO.listRooms.push(data._id)
                            socketIO.join(socketIO.listRooms)
                        }
                        socketIO.emit('is-successfully', { status: 200, message: "OK" })
                    } else {
                        //can't get userID
                        socketIO.emit('error-message', { status: 400, message: "Connection refused" })
                    }
                })
            })

            socketIO.on('start-chat', data => {
                var roomID = data.roomID || ""
                ChatroomDB.findById(roomID, (err, room) => {
                    if (err)
                        socketIO.emit("error-message", { message: err.message, status: "failed" })
                    if (!room) {
                        socketIO.emit("error-message", { message: "NOT_FOUND", status: "failed" })
                    } else {
                        if (String(room._id) != String(socketIO.rooms["chat"])) {
                            socketIO.leave(socketIO.rooms["chat"])
                            socketIO.roomID = room._id
                            socketIO.rooms["chat"] = room._id
                            socketIO.listRooms = [room._id, socketIO.rooms["user"] || socketIO.currentUser || null]
                            socketIO.join(socketIO.listRooms)
                        }
                    }
                })
            })

            socketIO.on("message", data => {
                socketIO.to(socketIO.rooms["chat"]).emit('message', data)
                var str = data.message || ""
                var img = data.img || ""
                var newMessage = {
                    createdTime: moment().format(),
                    message: str || "",
                    img: img || "",
                    userID: socketIO.currentUser || data.userID || "",
                    roomID: socketIO.roomID,
                    read: [{ "_id": socketIO.currentUser || data.userID || "" }]
                }
                MessageDB.create(newMessage, (err, mess) => {
                    if (err) socket.emit("error-message", { message: err.message, status: "failed" })
                })
                ChatroomDB.update_time(socket.roomID)
            })


            socketIO.on('like', dataLike => {
                likeController.like(dataLike, (err, like) => {
                    /**
                     * @params of dataLike
                     * 
                     * target : creator post
                     * postID
                     * token
                     */
                    if (String(dataLike.target) != String(like.currentUser._id)) {
                        if (like) {
                            socketIO.to(dataLike.target).emit("like", { like: true, postID: dataLike.postID, isComment: false })
                            var newNotify = {
                                isLike: true,
                                isComment: false,
                                isTag: false,
                                postId: like.newLike.postID,
                                title: `${like.currentUser.name} đã like vào bài post của bạn`,
                                userAction: { _id: like.currentUser._id, name: like.currentUser.name, avatar: like.currentUser.avatar },
                                isRead: false,
                                createdTime: moment().format(),
                                targetUser: dataLike.target
                            }
                            notifyController.create(newNotify, (err, noti) => {
                                // console.log(err, noti)
                            })
                        }
                    }
                })
            })

            socketIO.on('comment', dataComment => {
                commentController.create(dataComment, (err, comment) => {
                    /**
                     * @params of dataCmt
                     * 
                     * target : creator post
                     * postID
                     * token
                     * content
                     */
                    if (String(dataComment.target) != String(comment.currentUser._id)) {
                        if (comment) {
                            socketIO.to(dataComment.target).emit("comment", { like: false, isComment: true, postID: dataComment.postID })
                            var newNotify = {
                                isLike: false,
                                isComment: true,
                                isTag: false,
                                postId: comment.newComment.postID,
                                title: `${comment.currentUser.name} đã bình luận vào bài post của bạn`,
                                userAction: { _id: comment.currentUser._id, name: comment.currentUser.name, avatar: comment.currentUser.avatar },
                                isRead: false,
                                createdTime: moment().format(),
                                targetUser: dataComment.target
                            }
                            notifyController.create(newNotify, (err, noti) => {
                                // console.log(err, noti)
                            })
                        }
                    }
                })
            })

            socketIO.on('exit', (data) => {
                socketIO.leave(data.roomID)
            })

        } else if (socketIO.handshake.headers["x-apikey"] === "am1zLXNlcnZpY2UtY29ubmVjdGVk") {
            //JMS connect
            socketIO.on('mission', data => {
                socketIO.broadcast.emit(data.target + "/missions", { data: data })
            })

            socketIO.on('level', data => {
                socketIO.broadcast.emit(data.target + "/level", { data: data })
            })

        } else {
            console.log("Wrong key")
            socketIO.emit('error-message', { status: 500, message: "CONNECTION REFUSED" })
        }
    })

    socket.on('connection', data => {
        socket.removeAllListeners();
        console.log("DTMS is started");
    })

    socket.on('error', data => {
        console.log(data)
    })

}

module.exports = {
    listener,
    init
}