var moment = require('moment')
var missionDB = require('../../models/mission')
var userDB = require('../../models/user')

exports.mini_level = async(mission, next) => {
    await userDB.getFromId(mission[0].currentUser, (err, usr) => {
        if (usr.level == 2) {
            var newExp = mission[0].experience + usr.experience
            var newCoin = mission[0].coin + usr.coin
            userDB.updateUser({ _id: mission[0].currentUser }, { experience: newExp, coin: newCoin }, (err, newUser) => {
                if (err)
                    return next(err)

                if (newUser) {
                    var missionLog = {
                        missionID: String,
                        userID: String,
                        status: String,
                        completedTime: Date
                    }
                    return next()
                } else {
                    return next(["Không update được user"])
                }
            })
        }
    })
}