var dbConfig = require('../config/db')
var mongoose = require('mongoose')
var moment = require('moment')

var JobsSchema = mongoose.Schema({
    data: {},
    status: String,
    createdTime: Date,
    keys: [],
    currentUser: String,
    action: String,
    missionID: String,
    listErrors: []
})

var JobsConsumedSchema = mongoose.Schema({
    data: {},
    status: String,
    createdTime: Date,
    keys: [],
    currentUser: String,
    action: String,
    completedTime: Date,
    listErrors: [],
    excuteTime: Number
})

JobsSchema.index({ keys: 1 })
JobsSchema.index({ status: 1 })
JobsSchema.index({ data: 1 })
JobsSchema.index({ missionID: 1 })

JobsConsumedSchema.index({ keys: 1 })
JobsConsumedSchema.index({ status: 1 })
JobsConsumedSchema.index({ data: 1 })

var JobModel = dbConfig.initDBLog.model("job", JobsSchema)
var JobConsumedModel = dbConfig.initDBLog.model("job_consumed", JobsConsumedSchema)


exports.create = (data, cb) => {
    JobModel.create(data, (err, job) => {
        return cb(err, job)
    })
}

exports.getJobs = (_id, cb) => {
    JobModel.findById(_id, (err, job) => {
        return cb(err, job)
    })
}

exports.updateJob = (query, data, cb) => {
    JobModel.updateOne(query, data, (err, job) => {
        if (err) {
            return cb(err)
        } else {
            JobModel.findById(job._id, (err, j) => {
                return cb(err, j)
            })
        }
    })
}

exports.consumedJob = async(id, data, timeStart, cb) => {
    var end = moment().unix() - timeStart
    var jobConsumed = {
        data: data.data,
        status: "COMPLETED",
        createdTime: data.createdTime,
        keys: data.keys,
        currentUser: data.currentUser,
        action: data.action,
        completedTime: moment().format(),
        listErrors: data.listErrors || [],
        excuteTime: end
    }

    await JobConsumedModel.create(jobConsumed, (err, j) => {})

    await JobModel.deleteOne({ _id: id }, (err, job) => {})
}